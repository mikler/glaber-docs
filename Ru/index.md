# Документация  

## [Обшая информация, функционал, системные требования](./general/)
## [Установить Glaber](./setup/)
## [Администрирование](operations)

<!-- ## Эксплуатация и системные требования
## Администрирование 
## Спцецифичные шаблоны -->
## [Ключевые функции](features)
## [Расширить возможности Glaber](extending)
## [Примеры использования](/Glaber_docs/ru/Сценарии%20применения/Usecases)
## [Release notes / changelog](releasenotes)
## [Правила сборки и изменения проекта](developing)

<!-- 
# **English documentation**
# Glaber repositories for most popular Linux OS:

 


[Asynchronous AGENT pollers](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/en/Async-agent-polling)

# **Документация на русском языке**



## Установка Glaber

## [Хранение истории и трендов](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/Glaber-2.x-History/Glaber-2.x-History)

[**История в Clickhouse и модуль glb_hist_clickhouse**](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/Glaber-2.x-History/Clickhouse-%D0%B8-%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D1%8C-glb_hist_clickhouse)

* [Особенности настройки ClickHouse](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/Glaber-2.x-History/%D0%BE%D1%81%D0%BE%D0%B1%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8-%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-ClickHouse)
* [Апгрейд истории в ClickHouse c Glaber 1.x](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/Glaber-2.x-History/Upgrading-from-Glaber-1.x)

[**История в VictoriaMetrics и модуль glb_hist_victoria**](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/Glaber-2.x-History/%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D1%8C-glb_hist_victoria)

## Воркеры

## Поллинг (сбор метрик)

* [Асинхронные поллеры Агентов](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/%D0%90%D1%81%D0%B8%D0%BD%D1%85%D1%80%D0%BE%D0%BD%D0%BD%D1%8B%D0%B9-%D0%BF%D0%BE%D0%BB%D0%BB%D0%B8%D0%BD%D0%B3-%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D0%BE%D0%B2)
* Асинхронный поллинг SNMP
* Высокопроизводительный ping
* Внешние скрипты - воркеры
* Воркеры в режиме "сервера"

## [Кеш метрик](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/ValueCache-persistancy)

## Препроцессинг

* [Агрегация в препроцессинге](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ru/%D0%9F%D1%80%D0%B5%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D1%81%D1%81%D0%B8%D0%BD%D0%B3/%D0%90%D0%B3%D1%80%D0%B5%D0%B3%D0%B0%D1%86%D0%B8%D1%8F-%D0%B2-%D0%BF%D1%80%D0%B5%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D1%81%D1%81%D0%B8%D0%BD%D0%B3%D0%B5)

## Оптимизация работы с UI

* последние данные (lastvalues)

-->
