# Что нового в Glaber3.0

## Исходный код

- Обновлен на Zabbix 6.4 beta3

Изменения в функционале Zabbix можно посмотреть в дорожной карте https://www.zabbix.com/roadmap, так как пока это бетта - часть из запланированного еще не реализована.

В Glaber отключен функционал `inline host editing` от 6.4 версии, позволяющий редактировать хост в всплывающем окне, так как пользовательский опыт с ним ухудшился.


!!! Примечание 

    Обновление на стабильную версию исходников Zabbix планируется, как она выдет и будет стабильной. Такое обновление планируется после версии Glaber3.3.x, в которой будет полностью убрана зависимость от библиотеки `zbxdbcache` (функционал `HistoryCache`, `ValueCache` )

## Перенесены интерфейсы в модули

 - Панель последние данные - `Latest data` перенесана в модули UI. После обновления по умолчанию будет стандартная панель последних данных. 
  
Функционал у панелей разный: стандартная дает возможность фильтрации по тегам, панель от Glaber позволяет использовать живой динамический поиск, видеть предпросмотр графиков, а также группировать метрики в таблицы по Discovery данным.

 - Панель отладки перенесена в модули UI. После обновления на Glaber3.x при необходимости отладки по элементу данных или триггеру, ее нужно включить.

[Инструкция по активации и деактивации модулей](../features/modules/index.md)

## Cистема релизов

Доступны пакеты для трех версий, релизной `release`, тестовой `testing` и экспериментальной `experimental`. Подробнее в [описании репозитариев](../setup/repos.md)

## Восстановлена работа статусов интерфейсов

Статусы интерфейсов работают в том числе при использовании асинхронных поллеров. Статусы вынесены из SQL базы, поэтому более не влияют на нагрузку базы и скорость обработки. 

Расширено отображение статуса интерфейсов: дополнительно указывается время, когда интерфейс последний раз обновил статус

![](images/iface_agent.png)

![](images/iface_snmv3_fail.png)

## Триггеры

Изменена логика подсчета и вычисления триггеров, хранение статуса триггеров вынесено из SQL базы. 

!!! Примечание
    Несмотря на изменение логики, общий функционал не поменялся и вычисление проблем и событий осталось прежним. Суть изменений: теперь значение триггера _не_связано_ с наличием или отсутствием открытых проблем. Триггер всегда равен тому значению, в которое он был вычислен

Подробнее логика работы триггеров и состояний [описана тут](../features/triggering/index.md)

В UI также теперь отображается значение триггера UNKNOWN в случае, если его невозможно посчитать. 

![](images/trigger_values.png)

Изменено отображение триггера при просмотре события: сделан блок с оперативными данными, где показываются текущее значение триггера и текущие значения элементов данных на основе которых он был вычислен, плюс время сбора данных и последнего обновления значения триггера.
![](images/event_details.png)

В UI также более нигде не используется путающее поле State, которое раньше выполняло функции оперативного состояния (работоспособность) триггера. Индикатором неработоспособности триггера, начиная с версии 3.x является значение триггера  `unknwon`

В API функционал State по прежнему работает, эмулируется из значения триггера

## Работа Worker 
Улучшена стабильность, worker переведен полностью на событийную модель

## Расширены функции для обработки логов

Добавлена функция препроцессинга, позволяющая искать узлы по IP адресам их интерфейсов и перенаправлять логи в метрики таких узлов. Это позволяет полноцеено работать с логами с устройств, где вместо имени используется IP адрес агента. 

Добавлено отображение логов вместе с severity триггеров: 

![](../usecases/logs/../images/logs_colorisation.png)

Полностью описание настройки логов можно посмотреть в [Примерах использования: Логи](../usecases/logs.md)

## Обновление с Glaber2.x

Обновление проходит автоматически. Установите новые пакеты, перезапустите сервер. Сервер автоматически обновит базу данных. Процесс обновления можно контролировать по лог файлу сервера. 

Изменений в базе Clickhouse от версии 2.x нет, база прямо и обратно совместима с версией 3.0.
