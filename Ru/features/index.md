# Ключевые функции и подсистемы Glaber

## [Воркеры (workers)](workers)
## [Модули UI](modules)
## [API](api)
## Препроцессинг [агрегация](./preprocessing/aggregation.md), [маршрутизация по JSON данным](./preprocessing/jsondispatching.md)
## [Триггеринг](./triggering/index.md)