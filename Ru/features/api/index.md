# Glaber API

## Стандартное API
АPI в Glaber в точности соответсвует [API заббикс](https://www.zabbix.com/documentation/current/en/manual/api). Реализация работы с с объектами аналогична, даже если реализация в Glaber отличается.

## Расширенное API
Управление [связями](../dependencies/index.md) между узлами: [hostdepends](./hostdepends.md)
