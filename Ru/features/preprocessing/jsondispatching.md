# preprocessing: Items dispatching

Items dispatching allows to "redirect" item to another host and item. 

This is typically required for data coming from Server workers: [Server workers](/Glaber_docs/en/Workers/ServerWorkers.md).

Items dispatching takes two params: field name for host value  and metric name:
![](2022-08-25-12-07-58.png)

1. First param is used to find a host name in incoming JSON data. Value might be in several formats:

    Plain text:
   ~~~
   proc_id
   ~~~
    JSON path identification:
   ~~~
   $.proc_id
   ~~~
   
   
 Also field supports prefixed/suffixed notation, then field name should be enclosed in brackets **{}**:
   ~~~
   metric.{proc_id}.name.test.com
   ~~~
This allows very basic data field conversion done in-House. Only the first field name in brackets is processed.

Incoming JSON will be search for the field with name 'proc_id' and it's value will be used to do a host lookup. Host name is searched by 'Host' value, not 'Name'. Also search is case-sensitive.

If incoming data has host information in first level fields, use plain text, if the information is inside other objects or array, utilize the power of JSON path syntax:

Example: 
~~~
{ 
    ...
    "host":nil,
    "sd": {
        "hostinfo":"my-server-host",
        "user":"#234524352",
        ....
    }
    ....
}
~~~
in such a case to send the metric to the host 'my_server_host' use JSON path:
~~~
$.sd.hostinfo
~~~

If there is a need to postfix the server name, for example, add domain, use something like:

~~~
{$.sd.hostinfo}.user-server.test.loc
~~~
to search and route metric to host
~~~
my-server-host.user-server.test.loc
~~~



2. The second param has a static text value - item name.  From the previous example the result of the rule will be search of host 'my-server-host.user-server.test.loc', then metric with name 'log' is searched, if found, then item is send to preprocessing again for the new item.


## Further rule processing

Rule has **inverse** fail logic - if a metric has been redirected, it returns FAIL to stop firther processing. 

If a **host:metric** hasn't been found or other errors had happen, rule successes and processing continues, allowing to try another redirects or other kind of processing for non-redirected metrics: 

For example you might want to setup an [Aggregation count rule](/Glaber_docs/en/preprocessing/Aggregation.md) to count for non redirected metrics and setup a trigger to signal that there are a lot of data coming for unknown hosts.

Or for non-existing hosts preprocessing [JSON items Discovery rule](/Glaber_docs/en/preprocessing/JSONitemsDiscovery.md) might be used to create absent hosts.