# Миграция исторических данных из Postgres/Mysql БД в ClickHouse

Clickhouse имеет [встроенные движки для популярных баз данных](https://clickhouse.com/docs/en/engines/table-engines/integrations) которые можно использовать для импорта данных. 

Миграцию можно выполнить через выполнение SQL команд на Clickhouse сервер, ниспользуя утилиту `clickhouse-client`.

Мигрировать необходимо 6 основных таблиц: 

- четыре таблицы истории: 

    - `history`
    - `history_uint`
    - `history_str`
    - `history_text`

-  две таблицы трендов

      -  `trends`
      -  `trends_uint`

!!! Примечание
    В Glaber в ClickHouse и текстовые и строковые данные хранятся в единой таблице `history_str`

Ниже приведена последовательность команд для миграции с Postgres.

!!! Примечание
    
    В последовательности SQL команд исправьте данные для подключения к серверу Postgtes: имя пользователя и пароль, название базы и именование схемы.

``` 
clickhouse-client --password glaber

INSERT INTO glaber.history_dbl SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	'',
	'',
    ns,
    value
FROM PostgreSQL('host:port', 'database', 'history', 'user', 'password', 'schema')


INSERT INTO glaber.history_uint SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	'',
	'',
    ns,
    value
FROM PostgreSQL('host:port', 'database', 'history_uint', 'user', 'password', 'schema')

INSERT INTO glaber.history_str SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	'',
	'',
    ns,
    value
FROM PostgreSQL('localhost:5432', 'zabbix', 'history_str', 'zabbix', 'zabbix')

INSERT INTO glaber.history_str SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	'',
	'',
    ns,
    value
FROM PostgreSQL('host:port', 'database', 'history_text', 'user', 'password', 'schema')

INSERT INTO glaber.history_log SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	logeventid,
	source,
	severity,
	'',
	'',
    ns,
    value
FROM PostgreSQL('host:port', 'database', 'history_log', 'user', 'password', 'schema')

INSERT INTO glaber.trends_dbl SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	value_min,
	value_max,
	value_avg,
	num,
	'',
	''
FROM PostgreSQL('host:port', 'database', 'trends', 'user', 'password', 'schema')

INSERT INTO glaber.trends_uint SELECT
    toDate(clock) AS day,
    itemid,
    clock,
	value_min,
	value_max,
	value_avg,
	num,
	'',
	''
FROM PostgreSQL('host:port', 'database', 'trends_uint', 'user', 'password', 'schema')
```
  
Для `mysql` команды будут аналогичными, но дополнительно нужно будет поменять название движка БД на `MySQL`. 

P.S: Инструкция составлена на основе данных, предоставленным пользователем [@weduser](https://t.me/weduser).