Если впервые ставите ClickHouse, начните с официального гайда [https://clickhouse.yandex/docs/en/getting_started/](https://clickhouse.yandex/docs/en/getting_started/)

**Основные поинты настройки кликхауса:**

- использовать performance CPU scaling governor
- отключать transparent huge pages
- по возможности отключить swap
- можно использовать HDD, лучше выбрать RAID-10
- рекомендуемая файловая системы ext4
- вынести ОС на отдельный от ClickHouse диск, чтобы система не тормозила, если с ClickHouse будут проблемы
- выключить или урезать по времени храниния query_log таблицы. Гайд тут [https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-settings-to-adjust](https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-settings-to-adjust) 
- ознакомиться с https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-system-tables-eat-my-disk/

**(Внимание - будущих установщиков clickhouse для glaber
в этой доке Altinity  - [https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-settings-to-adjust](https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-settings-to-adjust) в первом же куске кода, нужно закоментировать строку `<!--<partition_by remove="1"/>-->` ибо clickhouse не запустится)**

**В плане настройки кликхауса надо не забыть:**

- сделать таблицы, SQL запросы для создания находятся в [https://gitlab.com/mikler/glaber/-/blob/master/database/clickhouse/history.sql](https://gitlab.com/mikler/glaber/-/blob/master/database/clickhouse/history.sql))
- в таблицах можно и нужно указать TTL, чтобы подчищать данные по расписанию, Удаление старых history и trends через HouseKeeper не будет работать. TTL должно соответсвовать настройкам глобальных таймингов для хранения истории в интерфейсе фронтенда.
- сделать отдельного юзера в кликхаусе, в users.xml для него настроить профиль

**В случае настройки кластера / реплик:**

- потребуется несколько таблиц: Distributed (в которую будем писать и из которой будем читать) и ReplicatedMergeTree (куда данные будут попадать из Distributed и где они будут храниться)
- если будет кластер, то нужен будет Zookeeper (хорошо если он уже есть, а вот если нет - надо будет настроить. По возможности не держать Zookeeper на тех же серверах что и кликхаус, но если он настраивается только под Glaber, то не страшно. Как еще один вариант - если Zookeepre на этих же серверах, то можно вынести его на SSD того же сервера. Зукипер надо будет настроить,  основные рекомендации по тюнингу есть в документации, там же есть конфигурация Zookeeper:
[https://clickhouse.tech/docs/ru/operations/tips/](https://clickhouse.tech/docs/ru/operations/tips/)