# Пакетные репозитории

## Поддержка операционных системы
На текущий момент существуют пакеты под следующие дистрибутивы:
Debian (Buster, Bullseye), Ubuntu (Bionic, Focal),
Centos 8, Astra Linux Smolensk

## Ветки репозиториев
Сущетсвуют три ветки репозиториев:

### release
Последний релиз, предназначен для продуктивного использования. 

Репозитории находятся по адресу [https://glaber.io/repo](https://glaber.io/repo)

### testing 
Репозитории со сборками с новыми функциями. Работоспособность в testing была проверена на тестовых средах и тестах но в продуктивном 
использовании такие системы использовать не рекомендуется. Версии в testing улучшаются и поддерживаются до попадания в стадию
release.

Репозитории находятся по адресу [https://glaber.io/repo-testing](https://glaber.io/repo-testing)

### experimental
В такие репозитории попадают кастомные сборки, для проверки гипотез, получения отладки. Этот репозиторий не нужно использовать и он может
содержать неподдерживаемые и неработающие сборки ПО. Иногда в процессе решения и отладки команда Glaber может попросить поставить вас пакеты из этого репозитария.

Репозитории находятся по адресу [https://glaber.io/repo-experimental](https://glaber.io/repo-experimental)


!!! Примечание

    Инструкции даны для `release` репозитория.

    Если используете `testing` или `experimental`, измените в URL репозитория и URL ключа адрес с `https://glaber.io/repo/` на `https://glaber.io/repo-testing/` или на `https://glaber.io/repo-experimental/` соответственно.

!!! Примечание для Astra Linux

    Пакеты Glaber для Astra Linux собраны с библиотекой net-snmp более новой версии, чем по умолчанию в системе и с принудительно включенным сильным шифрованием для версии SNMP v3. Поэтому для корректной работы Glaber *необходимо* [установить](astrasnmp.md) или [самостоятельно собрать](snmpv3sha2.md)  net-snmp пакеты, которые будут поддерживать библиотеку net-snmp нужной в необходимой версии и конфигурации. 
    


## Добавление репозиториев в Ubuntu:


Выполните команды с правами администратора:
~~~~
apt-get update && apt-get install wget gnupg2 lsb-release apt-transport-https -y 
wget --quiet -O - https://glaber.io/repo/key/repo.gpg | apt-key add -

echo "deb [arch=amd64] https://glaber.io/repo/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/glaber.list
apt-get update
~~~~

## Добавление репозиториев в Debian:
~~~~
apt-get update
apt-get install wget gnupg2 lsb-release apt-transport-https -y 
wget --quiet -O - https://glaber.io/repo/key/repo.gpg | apt-key add -

echo "deb [arch=amd64] https://glaber.io/repo/debian $(lsb_release -sc) main" > /etc/apt/sources.list.d/glaber.list 
apt-get update
~~~~

## Добавление репозиториев в Astra Linux:
~~~~
apt-get update
apt-get install wget gnupg2 lsb-release apt-transport-https -y 
wget --quiet -O - https://glaber.io/repo/key/repo.gpg | apt-key add -

echo "deb [arch=amd64] https://glaber.io/repo/astra smolensk main" > /etc/apt/sources.list.d/glaber.list 
apt-get update
~~~~

## Добавление репозиториев в Сentos:
~~~~
echo "[glaber]
name=Glaber Official Repository
baseurl=https://glaber.io/repo/rhel/\$releasever/
enabled=1
gpgcheck=0
repo_gpgcheck=0" > /etc/yum.repos.d/glaber.repo
~~~~

## Именование пакетов

Пакеты именуются с префиксом glaber вместо zabbix, например так

```
    glaber-server-pgslq 
    glaber-proxy-mysql
```
Перед установкой пакетов нужно удалить соответствующие zabbix* пакеты, так как имена исполняемых файлов используются прежние, например 
~~~~
    zabbix_server
~~~~
В пакетах Glaber поставляется ПО сервера, прокси, фронтенда. 

Агенты, сендеры, и прочее вспомогательное ПО нужно ставить из официальных пакетов zabbix, они полностью совместимы с Glaber.

## Установка пакетов 

Для установки используйте команды 

### в Ubuntu и Debian
~~~~
apt-get install glaber-server-mysql
~~~~

### в Centos
~~~~
yum install glaber-server-pgsql
~~~~
