# Настройка сервера Glaber на Ubuntu 20.0
## **Обновление**

Обновляем систему
~~~
apt-get update
apt-get upgrade
apt-get dist-upgrade
apt-get autoremove
~~~

## **Локализация**

Устанавливаем локали
~~~
dpkg-reconfigure locales
~~~
Отметить и поставить ru\_RU.UTF-8 и ru\_RU.ISO-8859-5\
Дефолтной в следующем окне поставить ru\_RU.UTF-8

## **Настройка времени**

Выставляем часовой пояс
~~~
timedatectl set-timezone Europe/Moscow
~~~
И настраиваем NTP
~~~
vim /etc/systemd/timesyncd.conf

NTP=192.168.1.1 
~~~
(вписываем свой шлюз или адрес желаемого NTP)
~~~
service systemd-timesyncd restart
~~~
Проверяем работу

~~~
timedatectl timesync-status
~~~

## **Настройка UFW (файервола)**

Настройка UFW, если он вообще включён. Должен быть следующий набор

~~~
ufw app list

Nginx Full

Nginx HTTP

Nginx HTTPS

OpenSSH
~~~

Если чего-то нет, то
~~~
ufw allow 'Nginx HTTP'
~~~

Работает ufw, или не работает, можно проверить командой

~~~
ufw status
~~~

## **Особенности Glaber, файлов, настройки**

Glaber это форк Zabbix с некоторыми отличиями. Для самой системы Ubuntu
он как бы остаётся приложением Zabbix, используются те же команды
service zabbix-server restart, используются конфигурационные файлы от
Zabbix и так далее.

При установке используется родной пакет от Zabbix для импортирования
параметров базы данных

В отличие от Zabbix, Glaber использует две базы данных. Одна (MySQL или
PostrgeSQL) - стандартная от Zabbix, используется для хранения настроек, вторая (Clickhouse, VictoriaMetrics или
InfluxDB) - используется для хранения истории как самого большого объёма
данных мониторинга. 

В Glaber используются собственные поллеры, их рассмотрим
в настройке ниже

## **Установка Glaber (на примере связки PostgreSQL + Nginx + Clickhouse)**

### **Ставим Glaber**

Glaber использует имена от Zabbix, соответственно нужно подсунуть
правильный репозиторий [https://glaber.io/repo](https://glaber.io/repo)

Предварительно ставим зависимости (в том числе Nginx и PHP)
~~~
apt install fontconfig-config fonts-dejavu fonts-dejavu-core
fonts-dejavu-extra fping libfontconfig1 libgd3 libjbig0 libjpeg-turbo8
libjpeg8 libmysqlclient21 libnginx-mod-http-image-filter libnginx-mod
http-xslt-filter libnginx-mod-mail libnginx-mod-stream libodbc1 libonig5
libopenipmi0 libpq5 libsensors-config libsensors5 libsnmp-base libsnmp35
libtiff5 libwebp6 libxpm4 mysql-common nginx nginx-common nginx-core php
bcmath php-common php-fpm php-gd php-ldap php-mbstring php-mysql php-xml
php7.4-bcmath php7.4-cli php7.4-common php7.4-fpm php7.4-gd php7.4-json
php7.4-ldap php7.4-mbstring php7.4-mysql php7.4-opcache php7.4-readline
php7.4-xml php-bcmath php-json
~~~
[Далее настраиваем репозитории](docs/ru/setup/repos)

~~~
Обновляем информацию о доступных пакетах

apt update
~~~

И ставим все пакеты и PostgreSQL

~~~
apt install glaber-server-pgsql glaber-nginx-conf glaber-frontend-php
php7.4-pgsql zabbix-sql-scripts zabbix-agent postgresql
~~~

## **Настраиваем БД PostgreSQL**

Создаём пользователя zabbix и базу zabbix, задаём желаемый пароль

~~~
sudo -u postgres createuser --pwprompt zabbix

sudo -u postgres createdb -O zabbix zabbix
~~~
Удалить БД можно так (на всякий случай)
~~~
sudo -u postgres dropdb zabbix
~~~

Импортируем схему для БД Zabbix в PostgreSQL
~~~
zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | sudo -u zabbix
psql zabbix
~~~

Конфигурим настройки сервера Glaber

~~~
vim /etc/zabbix/zabbix_server.conf

DBName=zabbix

DBUser=zabbix

DBPassword=zabbix
~~~

**Настраиваем PHP**
~~~
vim /etc/php/7.4/fpm/php.ini

memory_limit = 512M
post_max_size = 64M
max_execution_time = 60
upload_max_filesize = 32M
max_input_time = 60
date.timezone = Europe/Moscow
~~~

**Настраиваем Nginx**
Основная настройка /etc/nginx/nginx.conf

~~~
vim /etc/nginx/nginx.conf
~~~
В блоке http вписываем
~~~
client_max_body_size 32M;
~~~
Настройка сайта для фронтенда: /etc/zabbix/nginx.conf
~~~
vim /etc/zabbix/nginx.conf
~~~
Раскомментируем порт и вписываем адрес сервера
~~~
listen 80;

server_name glaber.yourcompany.loc glaber;
~~~
Анлинкуем дефолтный сайт, чтобы вместо заглушки Nginx у нас загружалась
страничка Glaber
~~~
unlink /etc/nginx/sites-enabled/default
~~~
Если вдруг надо линкнуть обратно, то
~~~
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/
~~~

## **Устанавливаем и настраиваем Clickhouse**
### **Ставим Clickhouse**

[Установка
Clickhouse](https://clickhouse.com/docs/en/getting-started/tutorial/)\
Импортируем репозиторий и ставим

~~~
apt-key adv --keyserver keyserver.ubuntu.com --recv E0C56BD4

echo "deb http://repo.yandex.ru/clickhouse/deb/stable/ main" | sudo
tee /etc/apt/sources.list.d/clickhouse.list

apt update
apt install clickhouse-server clickhouse-client
~~~

В процессе настройки задаём пользователя и пароль (можно оставить
default, только пароль придумать)

### **Тюнинг Clickhouse**

[Тюнинг](https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-settings-to-adjust/)

Создаём дополнительные файлы настройки с отключением лишних штук

Задаём ограничение времени хранения логов
~~~
vim /etc/clickhouse-server/config.d/query_log.xml

cat /etc/clickhouse-server/config.d/query_log.xml

<yandex>
    <query_log replace="1">
    <database>system</database>
    
    <table>query_log</table>
        <flush_interval_milliseconds>7500</flush_interval_milliseconds>
        <engine>
          ENGINE = MergeTree
          PARTITION BY event_date
          ORDER BY (event_time)
          TTL event_date + interval 90 day
          SETTINGS ttl_only_drop_parts=1
        </engine>
    </query_log>
</yandex>
~~~
Отключаем thread log
~~~
vim /etc/clickhouse-server/config.d/disable_query_thread_log.xml
~~~
~~~
<yandex>
    <query_thread_log remove="1"/>
</yandex>
~~~

Отключаем встроенные метрики Clickhouse если хотим мониторить его
параметры внешним мониторингом
~~~
vim /etc/clickhouse-server/config.d/disable_metric_logs.xml
~~~
~~~
<yandex>
  <metric_log remove="1" />
  <asynchronous_metric_log remove="1" />
</yandex>
~~~
Отключаем part_log
~~~
vim /etc/clickhouse-server/config.d/part_log.xml
~~~
~~~
<yandex>
    <part_log remove="1" />
</yandex>
~~~

Включаем log queries (в новых версиях Clickhouse включен по-умолчанию)
~~~
vim /etc/clickhouse-server/users.d/log_queries.xml
~~~
~~~
<yandex>
  <profiles>
    <default>
      <log_queries>1</log_queries>
    </default>
  </profiles>
</yandex>
~~~
Запись данных на диск сразу в отсортированном виде
~~~
vim /etc/clickhouse-server/users.d/enable_on_disk_operations.xml
~~~
~~~
<yandex>
  <profiles>
    <default>
      <max_bytes_before_external_group_by>2000000000</max_bytes_before_external_group_by>
      <max_bytes_before_external_sort>2000000000</max_bytes_before_external_sort>
     </default>
  </profiles>
</yandex>
~~~

### **Настройка Clickhouse для Glaber**

Создаём таблицы из файла
[https://gitlab.com/mikler/glaber/-/blob/master/database/clickhouse/history.sql](https://gitlab.com/mikler/glaber/-/blob/master/database/clickhouse/history.sql)
(при этом надо задать желаемый TTL хранения данных и синхронизировать
его с аналогичным настройками в веб-интерфейсе Glaber. По-умолчанию
данные хранятся 6 месяцев)

Создаём себе в любом месте файл history.sql и копируем туда содержимое с гита

Создаем БД
~~~
clickhouse-client --password --multiquery < history.sql
~~~

Конфиги Clickhouse хранятся тут (на всякий случай)

~~~
/etc/clickhouse-server/config.xml
/etc/clickhouse-server/users.xml
~~~

## **Настройка Glaber**

Есть
[пример](https://gitlab.com/mikler/glaber/-/blob/master/conf/glaber_server.conf)
файла с комментариями со специфичными настройками

~~~
vim /etc/zabbix/zabbix_server.conf
~~~

## **Сначала меняем стандартные настройки**

Нужно увеличить кеш для конфигурации, ориентир - 1,5G на каждые 1млн items, минимум 256Мб.
~~~
CacheSize=256M
~~~
dbsyncer в Glaber переваривает 20-30kNVPS, так что на большинстве
систем достаточно одного. Если вычисляется много триггеров, то следует увеличить. В среднем хватает одного синкера на 100тыс триггеров и 2млн метрик.
~~~
StartDBSyncers=1
~~~
Классические поллеры, они всё равно используются для некоторых задач
~~~
StartPollers=10
~~~
асинхронным поллерам не нужны поллеры недоступности, поэтому
оставляем совсем чуть-чуть на всякий случай

~~~
StartPollersUnreachable=1
~~~

Процесс чистки базы housekeeping очень легковесный в Glaber, можно
запускать как можно чаще

~~~
HousekeepingFrequency=1
~~~

В Glaber трапперы слушают два разных порта чтобы разделить нагрузку
мониторинга и UI/API, классические трапперы нужны для proxy, активных
агентов и snmp traps

~~~
StartTrappers=4
~~~

Классические пингеры могут быть полезны для старых железок, у которых
могут возникнуть проблемы с методом glbmap. А так же для localhost
~~~
StartPingers=2
~~~

## **Специфичные настройки для Glaber**

Воркеры - если используются воркер-серверы (прием логов, прием данных в режиме сервера по другим протоколам), то нужно запустить хотя бы один контроллирующий сервер:
~~~
StartGlbWorkers=1
~~~

### Асинхронный поллер SNMP
 один поллер может снимать метрики со скоростью около 10kNVPS. Рекомендуется запускать такое количество поллеров, чтобы на каждый приходилось не более 2млн метрик. Поллер использует один порт, но много соединений, может потребоваться увеличение conntrack таблиц фаервола.
~~~
StartGlbSNMPPollers=1
~~~

### Продвинутый опрос ICMP для Glaber
~~~
DefaultICMPMethod=glbmap
~~~
Если требуется вернуть fping: 
~~~
DefaultICMPMethod=fping
~~~
Больше одного запускать нет смысла, один поллер может отослать примерно 100-150тысяч пакетов в секунду.
~~~
StartGlbPingers=1
~~~

**ВАЖНО**: Пингер glbmap не поддерживает Vlan и маршрутизацию локального
компьютера. Нужно или ставить перед ним какой-то роутер, который будет
этим заниматься, или жёстко прописать работу через один Vlan, указав
номер интерфейса и мак маршрутизатора. Или использовать по старинке fping
~~~
GlbmapOptions=-i ens160 -G 00:50:56:9e:7c:9e
~~~
также для работы icmp через glbmap требуется указать, где находится утилита glbmap:

~~~
GlbmapLocation=/usr/sbin/glbmap
~~~

Пингеру Glaber нужны дополнительные права на запуск 
~~~
chmod +s /usr/sbin/glbmap
~~~

### Асинхронный поллер агентов. 

Один поллер держит 6-7kNVPS

~~~
StartGlbAgentPollers=1
~~~

### Отдельный траппер для UI и API
APITrappers используются для ответа на API запросы, опция позволяет разделить мониторинговую и пользователькую нагрузку на трапперы
~~~
StartAPITrappers=2
~~~

### Менеджер препроцессов
В Glaber можно запускать несколько менеджеров, из рассчета один на 40-50kNVPS
~~~
StartPreprocessorManagers=1
~~~
Сколько workers будет создано для кажного менеджера препроцессов
~~~
StartPreprocessorsPerManager=4
~~~


## **Настройка дополнительных путей и коннектов в БД**

С версии 2.9.0 вернулась встроенная поддержка clickhouse, поэтому
предпочтительнее использовать её вместо glb_hist_clickhouse
~~~
HistoryModule=clickhouse;{"url":"http://127.0.0.1:8123",
"username":"default", "password":"123456",
"dbname":"glaber", "disable_reads":100, "timeout":10 }
~~~


## Путь к папке с скриптами workers
~~~
WorkerScripts=/usr/lib/zabbix/workerscripts
~~~

**Настройка ValueCache**

Glaber перодически дампит кэш метрик, чтобы быстрее запускаться и
работать. Просмотр кеша иногда полезен при диагностике проблем. Указываем путь

~~~
ValueCacheDumpLocation=/tmp/vcdump
~~~

Частота дампинга. Значения 300 хватит для большинства инсталляций
~~~
ValueCacheDumpFrequency = 300
~~~

Нужно создать эту директорию и раздать права на неё
~~~
mkdir /tmp/vcdump/
chmod 777 /tmp/vcdump/
chown -R zabbix:zabbix /tmp/vcdump/ (возможно не нужно)
~~~

## **Настройка фронтена Glaber**
~~~
vim /etc/zabbix/web/zabbix.conf.php
~~~
Вписываем
~~~
global $HISTORY;
$HISTORY['storagetype']='server';
~~~

## **Запускаем все установленные сервисы**

### **Запускаем сервисы**

~~~
service clickhouse-server restart
service postgresql restart
service nginx restart
service php7.4-fpm restart
service zabbix-server restart
service zabbix-agent restart
~~~


### **Добавляем все сервисы в автозапуск**
~~~
systemctl enable nginx php7.4-fpm clickhouse-server postgresql
zabbix-server zabbix-agent
~~~

## **Завершаем инсталляцию Glaber**

Заходим на наш сервер по web-интерфейсу и проводим финишную настройку
Логинимся, пользователь по-умолчанию:

Логин: Admin
Пароль: zabbix

Дальше просто следуем инструкциям на экране
