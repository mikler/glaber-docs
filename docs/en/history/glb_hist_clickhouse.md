**Note:** \*ClickHouse is used only for history and trends storage. The configuration must still be stored in one of the SQL databases (PostgreSQL, MySQL, Oracle). \*

glb_hist_clickhouse is a Glaber worker written in Golang that implements the ability of Glaber to work with the ClickHouse history store.

It uses the HTTP interface for working with ClickHouse. Sources in [src/glapi](https://gitlab.com/mikler/glaber/-/tree/master/src/glapi).

## Module uses parameters:

**url** - http / https address where ClickHouse listens

**user** and **password** - credentials to use for ClickHouse server

**batch** - number of history lines to buffer before sending ClickHouse.

**flush** - the number of seconds after which to flush the accumulated history to ClickHouse. Healthy values are

batch and flush work according to the principle "whichever comes first" and are needed, on the one hand, to provide a comfortable load for ClickHouse (fewer updates with a large amount of data) and, on the other hand, not to make a significant delay in the data processing

\###**How to build glb_hist_clickhouse?** glb_hist_clickhouse comes with glaber_server_\* package and typically is placed into /usr/sbin/.

Sources as well as module dependencies files are located in the src/glapi directory. Just run "go build glb_hist_clickhouse" in src/glapi folder. Golang >= 1.14 or is required. Copy the resulting binary file, for example, to /usr/sbin.

To make the server using the worker, add it server configuration file. Example (in zabbix_server.conf)

HistoryModule = worker; {"path": "/usr/sbin/glb_hist_clickhouse", "params": "- url <http://127.0.0.1:8123?user=default&password=XXXX> -batch 100000 -flush 30", "disable_reads": 100000, "timeout": 5, "write_types": "dbl, str, uint, text", "max_calls": 10000000}

### **Configuring ClickHouse**:

[Tips on configuring ClickHouse](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/en/Configuring-ClickHouse)

### Storage Features

The structure is very similar to the history tables used in Zabbix. But there are several differences that distinguish the module from the standard storage method.

* by default, the module does not write nanoseconds for metrics. This saves a lot of space. This behavior can be changed by setting the module's parameter 'disable_ns' to 'false'
* by default, the module writes hostnames and metrics names. It doesn't really affect the disk space. But it will allow you to use the history even if the corresponding item has already been removed from the SQL database.

Please make sure you are using the [ValueCache persistency](https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ValueCache-persistancy) option to periodically dump Value cache, or use the "disalbe_reads":NN option to prevent ClickHouse hammering after server restart.