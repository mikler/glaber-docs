The module makes it possible to write metrics to the VictoriaMetrics database

The module supports only numeric data - integer and floating point.

##Features of the module, configuration

The module implements work with history (metrics) and trends. Despite the fact that VictoriaMetrics is able perfectly aggregate data, the trend storage functionality can be used to store trends in a separate VictoriaMetrics instance for a significantly longer period than the metrics. Let me remind you that trends are hourly aggregations of metrics.

Therefore, the most logical configuration is to launch two modules - one to use for history, the other for trends. Modules need to be configured for different "bases" and set up filtering of trends and metrics so that one receives only metrics, and the other only trends. An example is in the description of working with history.

Configuration is done by loading the history module:

bash
HistoryModule = worker; {"path": "/usr/sbin/glb_hist_victoria", "params":"-url http://127.0.0.1:8428 -batch 1000 -flush 3", "timeout": 5, "write_types": "dbl, uint" , "write_trend_types": "dbl, uint"}
``,

###The module supports parameters:

**dbname** (default "glaber") - the dbname tag is added to the metrics to implement an analog of the database name, it can be useful if several servers will write to one VictoriaMetrics servre to mix metrics in case their names or itemids are same.

**url** (default "[http://localhost:8428] (http://localhost:8428/)") - address where to write metrics. Port 8428 is default for VictoriaMetrics. If vmauth authorization proxy is used, then authorization parameters must be included in url

**batch** (default 10000) - how many records to save before sending the batch to the server. For VictoriaMetrics, buffering not as critical as for ClickHouse, but to reduce number of requests and increase efficiency, you should not significantly reduce the parameter.

**flush** (default 2) - time in seconds, how long to accumulate the buffer. Data is uploaded on the basis of "whichever comes first" - batch or flash. Do not set it too large - at this time there may be a delay in uploading metrics.


##Method module use to record historical data

glb_hist_victoria uses **Influx line protocol** in the format:

bash
item_123456,dbname=glaber, hostname=<hostname>,itemname=<itemname> value=123.345 <time>
``,

А tag is added to the metric - "base name", as well as symbolic host names and metrics.

This creates a metric in VictoriaMetrics named "item_123456_value"

##Method module use to record trends

Trends are written in a similar item format, but instead of one value, four values ​​are written - max, min, avg, count, which leads to the creation of four metrics for each itemid in Victoria and the suffix **trend _** is used:

trend_123455_max, trend_12345_min, _avg and _count respectively.

##Details on VictoriaMetrics interface use:
The module implements reading of trends and history, so the frontend will display charts using the data read functions /range_query.

/range_query is used for both history and trends.

The module uses the /export interface of VictoriaMetrics to get raw data.