- [Dedicate trapper for API](#dedicate-trapper-for-api)
  - [Reasoning, idea](#reasoning-idea)
  - [Configuration](#configuration)

# Dedicate trapper for API

## Reasoning, idea
As Glaber server serves user requests either, the separate process type **API trapper** is introduced.

API Trapper is intended to be used to serve API requests coming from the frontend and API and not interfere with **trapper** processes used to process metrics. 

The default API trapper port is 10055. On small setups, there is no need to separate trapper and API trapper. 

However, if there are a significant trapper load and quite a number of users using Glaber, then it's preferable to separate them and set numbers of the processes separately.

## Configuration
To enable API trappers and configure the number of processes running, use the following directives on the server config file:

~~~
StartAPITrappers</span> = 5
~~~
(default 0, APITrappers will be disabled)

To use a specific port, use:

~~~
`APIListenPort = 10055 `
~~~
(default 10055)

Change the port in API frontend configuration file:
~~~
$ZBX_SERVER_PORT = '10055';
~~~