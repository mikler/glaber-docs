Glaber is a fork of Zabbix, so basic installation is very similar to setting up Zabbix.

Please use official Zabbix guide to setup basic system for both frontend and backend. <https://www.zabbix.com/documentation/current/manual/installation>

Use glaber repositories and packages instead of zabbix ones.

More about Glaber packages could be found here.

# **Set up Glaber specific features**

## History Storage

Glaber comes with two history storage modules for ClickHouse and VictoriaMetrics databases.

### ClickHouse

ClickHouse supports all types of data storage, it's very efficient but requires a bit of configuration.

Setting up ClickHouse and database for Glaber: <https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/en/Configuring-ClickHouse>

Add ClickHouse configuration to the server config file:

```plaintext
HistoryModule=worker;{"path":"/usr/sbin/glb_hist_clickhouse", "params":"-url http://127.0.01:8123?user=default&password=XXXXXX -batch 100000 -flush 30", "disable_reads":1800, "timeout":5, "write_types":"dbl, str, uint, text, log", "max_calls":10000000 }
```

**_Note:_** _change ip for server and password to real ones._

ClickHouse module documentation <https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/en/History/glb_hist_clickhouse>

or

### VictoriaMetrics

Set up VictoriaMetrics storage. It's pretty straightforward to use, no configuration of VictoriaMetrics required unless you use some special setups, authorization, or cluster.

VictoriaMetrics is even more efficient in terms of resources, but only supports numeric data storage.

Just run VictoriaMetrics and configure the server by adding this configuration to the server config file:

```plaintext
HistoryModule=worker;{"path":"/usr/sbin/glb_hist_victoria", "params": "-url http://127.0.0.1:8086 -batch 1000 -flush 3"}
```

The module documentation <https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/en/History/glb_hist_victoria>

## ValueCache dumps

Set up valuecache dumps(<https://gitlab.com/mikler/glaber/-/wikis/Glaber-2.x-docs/ValueCache-persistancy>) for really fast server start:

```plaintext
ValueCacheDumpLocation=/var/lib/mysql/vcdump/valuecache_srv.dump
ValueCacheDumpFrequency = 120
```

## Fast pollers

Make sure there are ASYNC versions of AGENT poller, SNMP poller and ICMP pollers are running by enabling the options:

```plaintext
StartGlbWorkers=1
StartGlbSNMPPollers=1
DefaultICMPMethod=fping
StartGlbPingers=1
StartGlbAgentPollers=1
GlbmapLocation=/usr/sbin/glbmap
```

Note that classical pollers must remain either. There are some limitations for async pollers:

* async snmp pollers will not poll dynamic snmp items
* async agent pollers will not poll TLS enabled agent hosts

## On heavy loaded system, exceeding 40k NVPS start more then one PreprocessorManager:

```plaintext
StartPreprocessorManagers=2
```

Also note that since there might be more than one preprocessor manager in Glaber, the option to start PreprocessorManager workers is different:

```plaintext
StartPreprocessorsPerManager=4
```

As its name states, it sets how many workers are being run for _each_ preprocessor manager, not totally

# Frontend setup

Frontend needs direct access to the server's trapper interface, all features are working through the trapper, so the only configuration required for Glaber is to set option to pull history through the server:

```plaintext
global $HISTORY;
$HISTORY['storagetype']='server';
```

# Example of config file with Glaber specific options

<https://gitlab.com/mikler/glaber/-/blob/master/conf/glaber_server.conf>