# Dumping ValueCache to the hard drive

## what's the problem and the idea?

Anyone who restarted a highly loaded server saw a situation 
when for a long time after restarting the server "goes into itself", does not monitor, does not update the state of checks, and does not upload data to history. 

And it's quite possible that the server during massive problems will be restarted as a result of solving problems or to try to speed up the monitoring.

The main reason for slow performance at startup is filling the internal cache by metrics values from relatively slow history or SQL backend.

ValueCache is used to calculate triggers and calculated items.

Filling the ValueCache is done from two sources - from data that comes from monitoring and from data loaded from the history store.

To maximize efficiency on cache warm-up, in Glaber 2.x, a sort of "read ban" is implemented to avoid reading data if read operations took too long. In this case, the monitoring metric flow does not suffer, but it is likely that that some triggers, that need data in the cache, will not be calculated. And there is a chance of false triggering of "no data" checks.

Also, the load of the history store might be too high - from our experience, ClickHouse begins to significantly use the processor at the time of server restart.

## Decision

Flush ValueCache to disk periodically. On server startup, read it and "warm up the cache". One big massive quick operation.

## **How ​​it works**

Once in a certain period of time (by default - once every 60 seconds), the content of the ValueCache in json format is dumped into the file specified in the server configuration.

During the upload, a file with the .new suffix is ​​created, after the end of the upload, the file is renamed to the one specified in the configuration. This is done to avoid broken file when the server is stopped during a dump upload.

The dump is done by the HouseKeeper process. The unloading process does not block ValueCache, it buffers a batch of data in memory, and does long operations with the disk without any locks. Therefore, even if it takes a significant amount of time to write the cache to disk, it will not affect monitoring and will not cause blocking of any other parts of the server.

## Setting

There are two options that can be configured in the server configuration file for dumping the ValueCache.

1. A directory to save cache files - it is best to do this on a fast disk, for example NVME. Disk speed does not affect monitoring locking, but a fast disk causes fewer I / O wait events (iowait) and potentially problems for other processes.


``
ValueCacheDumpLocation = /tmp/valuecache/
``,

By default it's non and no dumps are created

2. Frequency of unloading in seconds

``
ValueCacheDumpFrequency = 60
``,

## Tests, results, experience, thanks

This feature is quite effective even if server has been switched off for significant amount of time. It's highly recommended to use it on any installation size.

 ### Thanks
 For the idea, elaboration, the opportunity to discuss and understand the usefulness - thanks to Andrey Gushchin. 