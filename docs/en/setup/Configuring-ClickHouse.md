**Tips on setting up  ClickHouse:** 

* use performance CPU scaling governor
* disable transparent huge pages 
* disable swap if possible 
* you can use HDD, it's better to choose RAID-10 
* recommended filesystem ext4 
* move the OS to a disk separate from ClickHouse so that the system does not slow down if there are problems with ClickHouse 
* disable or shorten the storage time of the query_log table. The guide is here <https://kb.altinity.com/altinity-kb-setup-and-maintenance/altinity-kb-settings-to-adjust> ( If you use the doc, please note - you'll need to comment out line <! - <partition_by remove = "1" /> -> because ClickHouse won't start) 

  **Prepare ClickHouse to accept data from Glaber**
* create tables, SQL queries to create are in <https://gitlab.com/mikler/glaber/-/blob/master/database/clickhouse/history.sql>. Please specify the desired data TTL in table creation queries.  HouseKeeper will not clean up data in ClickHouse, instead, it will be done automatically by Clickhouse according to specified TTL. Please make sure that TTL matches the global timing settings for storing history in the frontend. 
* make a user in СlickHouse, set up a profile for him in users.xml 

  **In the case of cluster/replica configuration**
* several tables will be required: 
  * Distributed (into which we will write and from which we will read) 
  * ReplicatedMergeTree (where data will go from Distributed and where it will be stored) if there is a cluster.

  In the case of a cluster, a Zookeeper daemon is needed.  If possible, do not keep Zookeeper on the same servers as the ClickHouse, but if it is used only for Glaber, then it might be ok. If Zookeeper is on the same servers with Clickhouse, you might want to put it on the SSD  Please consider reading the Zookeeper configuration there: <https://clickhouse.tech/docs/ru/operations/tips/>