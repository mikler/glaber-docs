# **Repositories**

Currently there are packages for the recent versions of popular linux flavors: **Debian, Ubuntu, Centos**

## Adding repositaries:
---
Adding repositories to Ubuntu:
~~~~
apt-get update && apt-get install wget gnupg2 lsb-release apt-transport-https -y &&
wget https://glaber.io/repo/key/repo.gpg &&
apt-key add repo.gpg && rm repo.gpg &&
echo "deb [arch=amd64] https://glaber.io/repo/ubuntu $(lsb_release -sc) main" >> /etc/apt/sources.list.d/glaber.list &&
echo "deb [arch=amd64] https://repo.utelecom.com.ua/ubuntu $(lsb_release -sc) main" >> /etc/apt/sources.list.d/glaber.list &&
apt-get update
~~~~
Adding repositories to Debian:
~~~~
apt-get update && apt-get install wget gnupg2 lsb-release apt-transport-https -y &&
wget https://glaber.io/repo/key/repo.gpg &&
apt-key add repo.gpg && rm repo.gpg &&
echo "deb [arch=amd64] https://glaber.io/repo/debian $(lsb_release -sc) main" >> /etc/apt/sources.list.d/glaber.list &&
echo "deb [arch=amd64] https://repo.utelecom.com.ua/debian $(lsb_release -sc) main" >> /etc/apt/sources.list.d/glaber.list &&
apt-get update
~~~~
Adding repositories to Centos:
~~~~
echo "[glaber]
name=Glaber Official Repository
baseurl=https://glaber.io/repo/rhel/\$releasever/
enabled=1
gpgcheck=0
repo_gpgcheck=0" >> /etc/yum.repos.d/glaber.repo

~~~~
---
## Package naming
Server packages: *glaber-server-pgsql, glaber-server-mysql*

