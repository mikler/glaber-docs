To be able to poll large infrastructures without making extra proxies just for scaling, Glaber can do very efficient SNMP polling using asynchronous methods. Such a performance is also useful when monitoring is done for remote equipment with big RTT (round-trip-time) even through satellite links.

Typically, one poller is sufficient for up to **100 thousand devices** or sustained poll rates of about **70-80k polls per second**.

That’s about 200-300 times more than a “normal” poller.

To start an async SNMP poller, use

`StartGlbSNMPPollers=1 (default 1, max 10)`

You might need more than one poller on systems with rather slow CPU cores. If the glb_snmp_poller process tends to constantly use more than 75% of a CPU core, increase the number of pollers. On a typical modern CPU, one is enough for most setups.

One SNMP poller is limited 32k simultaneous requests. Unlike agant poll, snmp pollers only use one socket and so one outgoing port for all the requests.

There are some limitations of async SNMP pollers:

*SNMP v3 is not supported yet. All such an items will be polled by a normal poller

* Async SNMP doesn’t use bulk requests even if the option is set. There are some hardware support issues as well as problem of determining a maximum number of OIDS asked in a single request which sometimes may cause request timeouts and overall poll speed degradation. 
