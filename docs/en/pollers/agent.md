**The idea and motivation**

To increase agent polling performance so avoid using additional proxies and resources.
 
**Implementation**

Asynchronous polling is included in the server and proxy code and does not require external workers, scripts, and other resources. 

Each asynchronous poller can simultaneously poll up to 8K agents. Pollers periodically synchronize the configuration to their internal queue, doing one-time macro resolving. DNS is not cached.

Requests to the same host are executed sequentially. Only agents without encryption are supported. IPV6 is supported. 

If asynchronous pollers are enabled, then all supported agents will be polled by asynchronous pollers, the rest - by classic synchronous pollers. When you turn off the asynchronous agents polling, all agents are processed by regular pollers.

**Configuration** 

Async agent polling is enabled by the option

```shell
StartGlbAgentPollers = 1 
```

in the server or the proxy configuration files. By default set to 0, async agent pollers are disabled. You can run a maximum of 10 async pollers. For most setups having one poller is sufficient. 

**Monitoring and performance** 

The load of the pollers could be controlled by the regular system load tools. Due to the serialization of requests, hosts with a large number of agent items may show polling delays. The process displays general performance parameters in the process name (proc title), they can be viewed through the **_ps, top_** commands.