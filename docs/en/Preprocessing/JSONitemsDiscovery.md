# preprocessing: JSON Items Discovery

## Reasoning
For high loaded systems it's usually not practical to do a Discovery from each incoming value. 

Instead it's better to aggregate the values for some time. Also since only part of incoming information is required for new objects creation, some filtering is required to reduce amount of data.

Example: Nginx servers produce logs with high rate. We want to have those servers to be created dynamicaly.

Servers are setup to log requests in JSON format and send their name in a field "servername":
~~~
{ "servermame":"nginx_node12", "client":"66.249.65.179", "time":"06/Nov/2014:19:10:38 +0600" ...}
{ "servermame":"nginx_node13", "client":"66.249.65.189", "time":"06/Nov/2014:19:10:39 +0600" ...}
{ "servermame":"nginx_node09", "client":"66.249.65.199", "time":"06/Nov/2014:19:10:39 +0600" ...}
{ "servermame":"nginx_node12", "client":"66.249.65.124", "time":"06/Nov/2014:19:10:40 +0600" ...}
.... <thousands rows of data> ....
{ "servermame":"nginx_node12", "client":"66.249.65.200", "time":"06/Nov/2014:19:11:00 +0600" ...}
~~~

The only valuable field for server creation here is "servername", so we define list of fields consisting form one field:
~~~
servername
~~~
and a timout of 300 seconds.

The first couple of seconds the rule recieves the stream of log data, it will pass through a JSON:
~~~
{ 
    {"servername":"nginx_node12"},
    {"servername":"nginx_node13"},
    {"servername":"nginx_node09"}
}
~~~
Then the same JSON will be repeated each 300 seconds. 

This way only three records required during Discovery processing, whatever the number of log records coming from the server.

So this value can go directly to dependand Discovery item with macro defined as 
{#SERVERNAME}=>"servername" which will lead to creation of three new servers with all required metrics.

## Using together with metrics redirection

It's possible to use the rule as a second rule after [JSON Items Dispatching](/Glaber_docs/en/preprocessing/JSONItemsDispatching.md). This way only non-existing hosts will get to the rule processing. However it should be noted that in such a configuration existing hosts or items will not be updated so they ought to have reasonalbe high TTL deletion values.

## Setup

The rule has two fields:
 1. list of fields in plaintext or JSON path format separated by semicolon
   ~~~
   proc_id;$.servername;site_id
   ~~~
 2. timeout for repeating the data 
   ~~~
   300
   ~~~   
Use the rule in preprocessing of discovery item or create discovery item dependand on worker server item that will have the ItemsDiscovery rule.

The latter is preferable for high-loaded setup to reduce internal data copying.