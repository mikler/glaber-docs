**glb_log_listener** 

Утилита была специальна написана для приема логов в JSON формате.

glb_log_listener слушает в формате Syslog (RFC3164), что позволяет отправлять на него данные прямо из nginx. Можно отправлять и из других приложений. Утилита автоматически собирает сообщения из нескольких сообщений, если нужно. Можно отправлять и из других прилдолжений, главное обеспечить формат JSON и поля, которые определяют как сервер ищет метрику.

glb_log_listener может принимать логи в двух форматах - "glaber_format" и стандартном, определенном в ngix, "combined" - формате.

Предпочтение стоит отдать первому, он содержит больше данных и возможностей для анализа работы, но если отсутсвует возможность переконфигурации серверов nginx, то можно пользоваться и стандартным форматом

Чтобы включить поддержку 'glaber_format', его нужно описать в контексте http опишем формат лога:

```bash
log_format  glaber_format '{"body_bytes_sent":$body_bytes_sent'
',"metadata":"nginx_log"'
',"bytes_sent":$bytes_sent'
',"host":"$hostname"' 
',"key":"$host"'
',"http_referrer":"$http_referer"'
',"http_user_agent":"$http_user_agent"'
',"remote_addr":"$remote_addr"'
',"request_length":$request_length'
',"request_method":"$request_method"'
',"request_time":$request_time'
',"request_uri":"$request_uri"'
',"server_protocol":"$server_protocol"'
',"status":$status'
',"upstream_response_time":"$upstream_response_time"'
',"time":$msec }';
```

И в конфигурации конкретного сайта добавить экспорт логов на адрес сервера с Glaber server:
```bash
access_log syslog:server=192.168.0.1:514 glaber_format;
```
Опции glb_log_listener 
- -listenUDP (по умолчанию 0.0.0.0:514) - адрес, на котором будет слушать логи в формате UDP
- -listenTCP (по умолчанию 0.0.0.0:514) - аналогично, но TCP
- -format ( по умолчению 'glaber_format'), можно указать  nginx_combined для стандартного nginx лога