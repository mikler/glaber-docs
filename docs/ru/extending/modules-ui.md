# Расширения API модулей

## управление контекстом и ссылками на модуль

В случае, когда модуль представляет значимую информацию и должен быть интегрирован в список ссылок объекта, он может это сделать путем передачи CallBack функции в обработчик ссылок **CContextLinksHelper**

Пример реализации: добавление в контекстное меню объекта узла ссылок на топологию: 

Получения объекта для управления ссылками:
```
$linksHelper = APP::Component()->get('links.context.handler');
```

Регистрация CallBack функции для отображения ссылки:

```
   $callback_func = function($data, $type, $context) {
        return "<a href='google.com'>Searching for type $type in context $context </a>";
    };

	$linksHelper->addLinksHandler('host', 'short', '100', $callback_func);
```
'host' - тип объекта

'short' - контекст отображения

'100' - порядок отображения, отображаются ссылки в возрастающем порядке


