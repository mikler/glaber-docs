## Введение

Glaber версии 1.4 был основан на ветке Zabbix 4.2. Вторая версия Glaber - на 5.2. Апгрейд SQL базы произойдет штатно, SQL база Glaber не 
имеет отличий от Zabbix, кроме того, что некоторые таблицы не используются.

Но нужно будет обновить базу в ClickHouse.
Формально сам Glaber не определяет формат хранения данных в бекенде истории, поэтому есть два пути перенести данные из 1.4 в 2.x

**Первый**, ему я уделять внимания буду мало - оставить прежней единую таблицу с историей и переписать запросы в утилите glb_hist_clickhouse так, чтобы она пользовалась единой таблицей. Это на ваше усмотрение, но стоит заметить, что в 2.x есть существенные отличия по функцоналу - во первых, пишутся тренды, во вторых, полноценная поддержка логов, а в таблице 1.4 логи полностью не поддерживаются. В общем, на ваш страх и риск и с пониманием что вероятно с прогрессированием фич в 2.x проблемы совместимости будут копиться можете просто переписать glb_hist_clickhouse

**Второй** - сконвертировать данные 

Немного про формат хранения данных в Glaber2x в Clickhouse:

- данные хранятся в четырех отдельных таблицах - целочисленные, с плавающей запятой, текстовые и логи. Основная мотивация разделить данные - обеспечить разное время хранения
- плюс есть отдельно две таблицы с трендами для целых чисел и значений с плавающей запятой. Тренды - это почасовая агрегация данных

Формат очень похож на способ хранения данных в Zabbix. Главное отличие - возможность писать имя хоста и имя метрики вместе с данными. Из за особенности хранения в СlickHouse повторяющиеся имена хостов и метрик практически не занимают места в отличии от классических SQL БД.

Немного про будущее - есть планы также добавить поле кода ответа, чтобы в истории сохранялись и данные о неудачных попытках снять данные и неудачных кодах ответов. 

## **Как сконвертировать данные:**

Экспорт и конвертация данных состоит из нескольких шагов: 

- выбор списков элементов по типам ( то есть выбрать списки itemid для uint, float, str и text и log данных соответственно)
- импорт данных в типизированные таблицы
- генерация трендов

### Выбор списков элементов данных по типам:

На примере postgres. В качестве клиента к БД - командная строка и psql клиента

Итак, для начала приведу названия типов и их численных констант из кода

```bash
ITEM_VALUE_TYPE_FLOAT = 0,
ITEM_VALUE_TYPE_STR = 1
ITEM_VALUE_TYPE_LOG = 2
ITEM_VALUE_TYPE_UINT64 = 3
ITEM_VALUE_TYPE_TEXT = 4
```

Различий в хранении истории между типом str и text нет, поэтому типы 1 и 4 обрабатываем вместе.

Итак, выгружаем в разные файлы itemid по типам (\o <имя файла> - выгрузка результата в файл): 

```bash
\o /tmp/dbl_itemid
select itemid from glaber.items where value_type = 0;
\o /tmp/uint_itemid
select itemid from glaber.items where value_type = 3;
\o /tmp/str_itemid
select itemid from glaber.items where value_type = 1 or value_type = 4;
```

Логи я не выгружаю, полноценной поддержки логов в версии 1.x не было

в итоге в трех файлах в каталоге /tmp будут содержаться списки itemid по типам: 

```bash
head -n 10 str_itemid
itemid
___________________
11597528
11541677
11541678
11541669
11541668
11541670
11597529
11597530
```

Готовим в Clickhouse структуры для новых таблиц: SQL запросы есть в каталоге database/clickhouse/history.sql. Если заливаете через командную строку clickhouse, то воспользуйтесь "--multiquery" параметром чтобы за раз залить все содержание фала. Перед заливкой обратите внимание на занчения времени жизни таблиц. Если нужно, скорректируйте. Именно это время, а не настройки в item или глобальные настройки во фронте будут определять, через какое время данные будут пропадать. По умолчанию время жизни истории выставлено в 6 месяцев, тренды не ограничиваются.

После создания таблиц нужно перелить туда данные (скопировать).

Идея перезаливки - выбираем из таблицы history старого формата данные по itemid определенного типа и копируем в новую таблицу.

Здесь может быть подводный камень в ограничении ClickHouse на количество элементов в запросе и на длину запроса. Поэтому если itemid много, то нужно их побить на куски

Сначала чистим файлы от мусора в конце и начала и приводим к виду, пригодному для заливки (моем случае psql генерирует 2 мусорных строки вначале и одну в конце): 

```bash
tail -n +3 str_itemid | head -n -2 | awk '{print $1 ","}' > str_prepared
tail -n +3 dbl_itemid | head -n -2 | awk '{print $1 ","}' > dbl_prepared
tail -n +3 uint_itemid | head -n -2 | awk '{print $1 ","}' > uint_prepared
```

Превращаем файлы с itemid в запросы: 

```bash
printf "set max_query_size = 51200000;\ninsert into glaber.history_str (day,itemid,clock,ns,value) select day,itemid,clock,ns,value_str from glaber.history where itemid in ("  | cat - /tmp/str_prepared > /tmp/str_convert.sql && echo ")" >>/tmp/str_convert.sql
printf "set max_query_size = 51200000;\ninsert into glaber.history_uint (day,itemid,clock,ns,value) select day,itemid,clock,ns,value from glaber.history where  itemid in ("  | cat - /tmp/uint_prepared > /tmp/str_convert.sql && echo ")" >>/tmp/uint_convert.sql
printf "set max_query_size = 51200000;\ninsert into glaber.history_dbl (day,itemid,clock,ns,value) select day,itemid,clock,ns,value_dbl from glaber.history where  itemid in ("  | cat - /tmp/dbl_prepared > /tmp/str_convert.sql && echo ")" >>/tmp/dbl_convert.sql
```

И через clickhouse-client выполняем (если нужны параметры авторизации или указания пользователя - добавьте): 

```bash
cat /tmp/str_convert.sql | clickhouse-client --multiquery
cat /tmp/uint_convert.sql | clickhouse-client --multiquery
cat /tmp/dbl_convert.sql | clickhouse-client --multiquery
```

Таблицы заполнены. Если на каком то этапе произошла ошибка, это не проблема - исходная таблица не меняется, поэтому можно попробовать заново, пересоздав или обнулив результирующую таблицу

## Генерируем тренды

По-сути операция похоже на предыдущую, просто с помощью insert into ...select генерируем почасовую аггрегацию по данным в таблицах с числовыми типами. SQL код нужно выполнить в clickhouse:

```bash
set max_query_size = 51200000;
insert into glaber.trends_uint 
	(day,itemid,clock,value_min, value_max,value_avg,count) 
SELECT  toDate(hour), 
	itemid, clock - (clock % 3600) AS hour, 
        min(value), max(value), avg(value), count(*) 
FROM glaber.history_uint group by itemid, hour

insert into glaber.trends_dbl
	(day,itemid,clock,value_min, value_max,value_avg,count) 
SELECT  toDate(hour), 
	itemid, clock - (clock % 3600) AS hour, 
        min(value), max(value), avg(value), count(*) 
FROM glaber.history_dbl group by itemid, hour

```