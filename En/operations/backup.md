# Бэкап и восстановление

Данные в Glaber хранятся в двух разных базах:
    - в SQL - конфигурация и объекты системы, логи, события
    - в timeseries хранилище - собранные метрики, как привило в качестве такого хранилища используется ClickHouse.

## Бекап и восстановление данных в SQL

### Mysql/Mariadb

Подробно процедуры бекапа и восстановления описаны на [https://mariadb.com/kb/en/backup-and-restore-overview/](https://mariadb.com/kb/en/backup-and-restore-overview/).

#### Бекап
Выполните команду (замените \<db_user\> и \<db_pass\> на имя кользователя и его пароль, имеющего доступ к БД glaber)
```
mysqldump -u<db_user> -p<db_pass> -E --single-transaction  glaber glaber.dump;
```
Эта команда сделает дамп базы данных glaber в файл glaber.dump, который нужно будет использовать в случае необходимости восстановить базу


#### Восстановление
Для восстановления нужно сначала создать пустую базу. Если база уже сущесвует, то сначала ее удалить. 

Если база используется и есть активный процесс сервера, то перед очисткой и пересозданием и восстановлением базы, нужно остановить сервер.

Используйте SQL команды 
```
mysql> DROP DATABASE glaber;
mysql> CREATE DATABASE glaber;
``` 

После создания базы на нее также нужно выдать права пользователю, или пользователям, от имени которых будет работать сервер и фронтенд:

Если пользователь не был заведен заранее, то создать его и задать пароль: 
```
mysql> CREATE USER '<db_user>'@'localhost' IDENTIFIED BY '<db_pass>';
```
И выдать пользователю права на базу

```
mysql> GRANT ALL PRIVILEGES ON glaber.* TO '<db_user>'@'localhost';
```
После чего через утилиту mysql залить бекап в новую базу: 
```
mysql -u<db_user> -p<db_pass> glaber < glaber.dump
```

### Postgres SQL

Подробно процедуры бекапа и восстановления описаны на [https://www.postgresql.org/docs/current/backup.html](https://www.postgresql.org/docs/current/backup.html)

#### Бекап
Выполните команду (замените \<db_user\> и \<db_pass\> на имя кользователя и его пароль, имеющего доступ к БД glaber)

```
/usr/bin/pg_dump --dbname=postgresql://<db_user>:<db_pass>@localhost:5432/$db  > glaber.dump;
```
Эта команда сделает дамп базы данных glaber в файл glaber.dump, который нужно будет использовать в случае необходимости восстановить базу

#### Восстановление
Для восстановления нужно сначала создать пустую базу. Если база уже сущесвует, то сначала ее удалить. 

Используйте SQL команды 
```
DROP DATABASE glaber;
CREATE DATABASE glaber;
``` 

После создания базы на нее также нужно выдать права пользователю, или пользователям, от имени которых будет работать сервер и фронтенд:

Если пользователь не был заведен заранее, то создать его и задать пароль: 
```
CREATE USER <db_user> WITH PASSWORD '<db_pass>';

```
И выдать пользователю права на базу

```
mysql> GRANT ALL PRIVILEGES ON glaber TO '<db_user>';
```
После чего через утилиту psql залить бекап в новую базу: 
```
psql -U <db_user> -d glaber < glaber.dump
```

## Бекап и восстановление ClickHouse
Glaber может работать с различными timeseries хранилищами. У каждог из них есть свои утилиты и методы выгрузки и бекапа данных. Clickhouse в настоящий момента - самая популярная и удобная система для Glaber.

Стоит учитывать, что объемы данных могут быть очень большими, поэтому зачастую нужно реализовывать миграцию данных по частям (по партициям, по таблицам)ю

Процедуры бекапа и восстановления в Clickhouse: [https://clickhouse.com/docs/en/operations/backup/](https://clickhouse.com/docs/en/operations/backup/)


#### Бекап
Настройте, куда сервер будет бекапить данные. Для этого нужно создать файл **/etc/clickhouse-server/config.d/backup_disk.xml**
с содержимым, указывающим место в файловой системе, куда будут попадать бекапы (в данном случае **/backups/**):
```
<clickhouse>
    <storage_configuration>
        <disks>
            <backups>
                <type>local</type>
                <path>/backups/</path>
            </backups>
        </disks>
    </storage_configuration>
    <backups>
        <allowed_disk>backups</allowed_disk>
        <allowed_path>/backups/</allowed_path>
    </backups>
</clickhouse>
```

Далее выполните бекап командой 
```
BACKUP DATABASE glaber TO Disk('backups', 'glaber.zip')
```

#### Восстановление
Настройте путь для работы с бекапами аналогично на сервере, на котором будут восстанавливаться данные, скопируйте в этот путь файл с бекапом и восстановите базу:

```
RESTORE DATABASE glaber FROM Disk('backups', 'glaber.zip')
```

