# Server workers

Server workers is an item type to accept data from outside systems passively, i.e. when data arrives without a specific item request. 

Server worker acts as a setup of data processing.

## Worker setup

Create an item of type "Server worker"
![](2022-08-25-11-32-25.png)

Select item type 'Server worker'

Fill the fields: 
![](2022-08-25-11-33-56.png)

Name: name of the item, name the item to be easily understandable.
Type: Server Worker
Key: any valid key format[link], also it's better use some [industry metric name convention](https://prometheus.io/docs/practices/naming/) for naming

**Worker path**: Absolute path (starting from /) or relative path to **WorkersDir** where the executable worker is stored. 

Worker command line parameters should be entered in the field as well.

**Timeout**: A server worker not being sending data after Timeout will be considered dead and restarted. 

Please use some large values to avoid false frequent restarts. If worker exists die to fail, Glaber will detect it immediately independent to Timeout settings

Rest of the fields are common, optional and self-explaining.

Upon saving the item and server loading the new configuration, the worker will start and all its output will be stored to the item's history

## Items dispatching 

Saving all input under the same item isn't practical. A typical server is supposed to gather metrics, logs, statistics from all the devices in the organization.

So after data has been received, it has to be dispatched to a host and an item it belongs to. 

To accomplish this data should have fields having information about the host it belongs to. This data might be prepared and checked in Worker program or might come directly from network.

To dispatch an item to the host use preprocessing rule "Dispatch to host from JSON value". 