# Documentation (translation in progress)

## [History](history)
## [General information, functionality, system requirements](general) (TBD)
## [Glaber setup](setup) (TBD)
## [Administration](operations) (TBD)

<!-- ## Эксплуатация и системные требования
## Администрирование 
## Специфичные шаблоны -->
## [Expand Glaber's capabilities](../docs/en/extending/) (TBD)
## [Usecases](../docs/en/usecases/) (TBD)
## [Release notes / changelog](https://gitlab.com/mikler/glaber/-/raw/master/ChangeLog.glaber)