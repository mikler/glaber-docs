# What's new in Glaber3.0

## Source code
- Updated to Zabbix 6.4 betta3

!!! Note
An update to the stable version of Zabbix sources is planned, as it will be released and will be stable. Also, such an update is planned after version 3.3, in which the dependency on the zbxdbcache library (HistoryCache, ValueCache functionality) will be completely removed

## Moved interfaces to modules
- The "latest data" panel has been moved to the UI modules. After the update, there will be a standard latest data panel by default. The functionality of the panels is different: the native one makes it possible to filter by tags, the Glaber panel allows you to use live dynamic search, see a preview of graphs, and also group metrics into tables based on Discovery data

- The debugging panel has been moved to UI modules. After updating to Glaber3, if debugging by data element or trigger is necessary, it must be enabled.

## Release system
Packages are available for three versions, release, test and experimental. Described in detail in [Release Schema](../set)

## Restored operation of interface statuses
The statuses of the interfaces work, they are taken out of the SQL database, so they no longer affect the load of the database and the processing speed. Expanded interface status display: additionally indicates the time when the interface last updated the status

## Triggers
The logic of counting and calculating triggers has been changed, the status of triggers has been stored from the SQL database

!!! Attention
Despite the change in logic, the overall functionality has not changed and the calculation of problems and events has remained the same. The essence of the changes: now the value of the trigger is not related to the presence or absence of open problems. The trigger is always equal to the value in which it was calculated

The UI also now displays the value of the UNKNOWN trigger in case it cannot be calculated.

The display of the trigger when viewing the event has been changed: a block with operational data has been made, where the current value of the trigger and the current values of the data elements on the basis of which it was calculated are shown, and the time of the last update of the trigger value.

## Job Worker
Stability has been improved, worker has been transferred completely to the event model